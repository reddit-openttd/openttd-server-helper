import {init as initBans} from './bans'
import { init as initScreenshots } from './screenshots'
import { init as initSaveGames } from './savegames'

export const init = () => {
  initBans()
  initScreenshots()
  initSaveGames()
}