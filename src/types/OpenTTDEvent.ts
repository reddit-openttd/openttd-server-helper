export type OpenTTDEventType = 'clientjoin' | 'clientquit' | 'chat' | 'chat-private'

export type HttpServerHost = {
  url: string
}

export type ConsoleEvent = {
  origin: string,
  output: string
}

export type ClientEvent = {
  clientId: number
}
export type Chat = ClientEvent & {
  message: string,
  money: number
}

export type OpenTTDEvent<T> = {
  serverHost: HttpServerHost,
  serverName: string,
  event: OpenTTDEventType,
  data: T
}