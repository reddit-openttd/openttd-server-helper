import dotenv from 'dotenv'
import * as openttd from './services/openttd'
import * as handlers from './handlers'

import sanityCheck from './sanity'

dotenv.config()

sanityCheck()

openttd.init()
handlers.init()
