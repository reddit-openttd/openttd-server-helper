import * as AWS from 'aws-sdk'
import {promisify} from 'util'
import { v4 as uuidv4 } from 'uuid';
// var AWS = require("aws-sdk");

const PK = 'PK$BAN'
const tableName = 'openttd'
const docClient = new AWS.DynamoDB.DocumentClient();

export type Banned = {
  ip: string,
  reason: string,
  name?: string
}

const createSK = (ip:string) => `SK$BAN$${ip}`
export const init = (options: any): Promise<void> => {
  return Promise.resolve()
}

export const removeIp = (ip: string) => {
  var params = {
    TableName: tableName,
    Key: { PK, SK: createSK(ip) }
  }
  return promisify(docClient.delete.bind(docClient))(params)
}

export const addIp = (banned: Banned) : Promise<void> =>  {
  const SK = createSK(banned.ip)
  var params = { TableName: tableName, Item: { PK, SK, ...banned  }}
  return promisify(docClient.put.bind(docClient))(params)
    .catch(err => {
      console.log(`${banned.ip} is already in the global ban list`)
      // var params = { TableName: tableName, Item: { PK, SK, ...banned  }}
      // return promisify(docClient.put.bind(docClient))(params)
    })
}

export const getAll = () : Promise<Array<Banned>> => {
  var params = {
    TableName: tableName,
    KeyConditionExpression: "PK = :PK",
    ExpressionAttributeValues: {
        ":PK": PK
    }
  }

  return promisify(docClient.query.bind(docClient))(params)
    .then(data => data.Items
      .map(item => {
        return {
          ip: item.ip,
          name: item.name,
          reason: item.reason
        }
      }))
}