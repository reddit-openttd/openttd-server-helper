import ottd from 'node-openttd-admin'
import events from 'events'
import { Chat, ConsoleEvent } from '../types/OpenTTDEvent'
import {GameInfo} from '../types/GameInfo'

const SERVER_ID = 0x01

const enums = ottd.enums

let _gameInfo: GameInfo = null
let connection = null
let rconSemaphore = Promise.resolve('')

export const openttdEmitter = new events.EventEmitter();

const reconnect = () => {
  connection.sock.connect(
    { 
      host: process.env.OPENTTD_HOST, 
      port: parseInt(process.env.OPENTTD_PORT) 
    }
  )
}
const start = () => {
  connection = new ottd.connection();
  connection.on('error', (err) => {
    if (err === 'connectionclose') {
      console.log(new Date().toLocaleTimeString() + ' Connection was closed. Restarting in 1 seconds.')
      setTimeout(() => reconnect(), 1000)
    }
  })
  connection.on('connect', function(){
    console.log('Connected to server.')
    connection.authenticate(process.env.OPENTTD_USER, process.env.OPENTTD_PASSWORD);
  });

  connection.on('welcome', async (gameInfo: GameInfo) => {
    _gameInfo = gameInfo
    connection.send_update_frequency(enums.UpdateTypes.CLIENT_INFO, enums.UpdateFrequencies.AUTOMATIC);
    connection.send_update_frequency(enums.UpdateTypes.CONSOLE, enums.UpdateFrequencies.AUTOMATIC);
    
    openttdEmitter.emit('welcome', _gameInfo)
  });

  connection.on('newgame', async (r) => {
    openttdEmitter.emit('newgame')
  })

  connection.on('shutdown', async () => {
    openttdEmitter.emit('shutdown')
  })
  connection.on('console', async (event: ConsoleEvent) => {
    openttdEmitter.emit('console', event)
  })

  connection.connect(process.env.OPENTTD_HOST, parseInt(process.env.OPENTTD_PORT) || 3977);
}

export const init = () => {
  start()
  return openttdEmitter
}

export const onNewGame = (handler: () => void) => {
  openttdEmitter.on('newgame', handler)
}

export const onWelcome = (handler: (gameInfo: GameInfo) => void) => {
  openttdEmitter.on('welcome', handler)
}

export const onShutDown = (handler: () => void) => {
  openttdEmitter.on('shutdown', handler)
}

export const onConsole = (handler: (event: ConsoleEvent) => void) => {
  openttdEmitter.on('console', handler)
}

export const getGameInfo = (): GameInfo => {
  return _gameInfo
}

export const sendPrivateMessage = (clientId: number, message: string) => {
  connection.send_chat(enums.Actions.CHAT_CLIENT, enums.DestTypes.CLIENT, clientId, message)
}

export const sendBroadcastMessage = (message: string) => {
  connection.send_chat(enums.Actions.CHAT, enums.DestTypes.BROADCAST, SERVER_ID, message)
}
export const getClients = () => {
  return sendAdminCommand('clients')
    .then(clientResponse => {
      const clients = clientResponse
        .split('\n')
        .filter(maybeEmptyString => maybeEmptyString)

      return clients.map(client => {
        //      'Client #1  name: 'server1'  company: 255  IP: server'
        const [_, num, _1, name, _2, company, _3, ip] = client.replace(/\s+/g,' ').split(' ')

        return {
          id: parseInt(num.replace('#', '')),
          name: name.replace(/'/g, ''),
          companyId: parseInt(company),
          ip
        }
      })
    })
}

export const getBanList = () => {
  return sendAdminCommand('banlist')
    .then(banList => {
      return banList.split('\n')
        .map(b => {
          const matches =  /\d+\) (.+)/.exec(b)
          if (matches && matches.length > 1) {
            return matches[1]
          }
          return null
        })
        .filter(b => !!b)
    })
}
export const sendAdminCommand = async (command: string): Promise<string> => {
  rconSemaphore = rconSemaphore.then(() => {
    return new Promise((resolve, reject) => {
      const timeoutId = setTimeout(() => reject('timed out waiting for rconend'), 5000)
      const outputs = []

      const rconOutput = ({colour, output}) => {
        outputs.push(output)
      }
      const rconEnd = () => {
        clearTimeout(timeoutId)
        removeHandlers()
        resolve(outputs.join('\n'))
      }
      const rconError = (err) => {
        clearTimeout(timeoutId)
        removeHandlers()
        reject('Somethings not working: ' + err)
      }
      const removeHandlers = () => {
        connection.removeListener('rcon', rconOutput)
        connection.removeListener('rconend', rconEnd)
        connection.removeListener('error', rconError)
      }
      connection.on('rcon', rconOutput)
      connection.on('rconend', rconEnd)
      connection.on('error', rconError)
  
      connection.send_rcon(command)
    })
  })

  return rconSemaphore
}