import { promisify } from 'util';
import AWS from 'aws-sdk';

export const uploadSaveGame = (serverName: string, fileName: string, buffer: Buffer) => {
  const s3 = new AWS.S3()
  const key = `save/${serverName}/${fileName}`
  var params = {
    Body: buffer, 
    Bucket: process.env.S3_BUCKET, 
    Key: key
  }

  return promisify(s3.putObject.bind(s3))(params)
}

export const uploadScreenShot = (serverName: string, fileName: string, buffer: Buffer) => {
  const s3 = new AWS.S3();
  const key = `screenshot/${serverName}/${fileName}`
  var params = {
    Body: buffer, 
    Bucket: process.env.S3_BUCKET, 
    Key: key
  }

  return promisify(s3.putObject.bind(s3))(params)
}